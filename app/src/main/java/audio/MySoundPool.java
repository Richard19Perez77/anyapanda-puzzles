package audio;

import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import anya.panda.puzzleimage.R;
import common.CommonVariables;

/**
 * 
 * Sound Pool extension that included loading sound and playing sounds.
 * 
 * @author Rick
 *
 */
public class MySoundPool extends SoundPool implements OnLoadCompleteListener {

	final int TAP = 1;
	final int CHIME = 2;
	CommonVariables commonVariables = CommonVariables.getInstance();

	@SuppressWarnings("deprecation")
	public MySoundPool(int maxStreams, int streamType, int srcQuality) {
		super(maxStreams, streamType, srcQuality);
	}

	public void playChimeSound() {
		// check for sound file to be loaded and wanting to be player
		if (commonVariables.chimeLoaded && commonVariables.playChimeSound)
			play(commonVariables.saveSound, commonVariables.volume,
					commonVariables.volume, 1, 0, 1f);
	}

	public void playSetSound() {
		// check for tap sound to be loaded and it in preferences
		if (commonVariables.tapLoaded && commonVariables.playTapSound)
			play(commonVariables.tapSound, commonVariables.volume,
					commonVariables.volume, 1, 0, 1f);
	}

	public void init() {
		setOnLoadCompleteListener(this);
		commonVariables.saveSound = load(commonVariables.context,
				R.raw.imagesaved, 1);
		commonVariables.tapSound = load(commonVariables.context, R.raw.tap, 1);
	}

	@Override
	public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
		if (sampleId == TAP)
			commonVariables.tapLoaded = true;
		else if (sampleId == CHIME)
			commonVariables.chimeLoaded = true;
	}
}