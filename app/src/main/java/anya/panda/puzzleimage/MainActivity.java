package anya.panda.puzzleimage;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import common.CommonVariables;

/**
 *  Start of applications.
 *
 *  Author: Richard A. Perez
 */

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "puzzleLog";
    CommonVariables common = CommonVariables.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if(common.isLogging)
            Log.d(TAG, "onCreate PuzzleFragment");

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(common.isLogging)
            Log.d(TAG, "onActivityResult  mainActivity");

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onStart() {
        if(common.isLogging)
            Log.d(TAG, "onStart mainActivity");

        super.onStart();
    }

    @Override
    protected void onRestart() {
        if(common.isLogging)
            Log.d(TAG, "onRestart mainActivity");

        super.onRestart();
    }

    @Override
    protected void onResume() {
        if(common.isLogging)
            Log.d(TAG, "onResume mainActivity");

        super.onResume();

/*        Animation anim = AnimationUtils.loadAnimation(this.getBaseContext(),
                R.anim.intro);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                common.isScreenAnimating = true;
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                common.isScreenAnimating = false;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        anim.reset();
        RelativeLayout container = (RelativeLayout) findViewById(R.id.container);
        container.clearAnimation();
        container.startAnimation(anim);*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if(common.isLogging)
            Log.d(TAG, "onCreateOptionsMenu myActivity");

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(common.isLogging)
            Log.d(TAG, "onCreateOptionSelected myActivity");

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        if(common.isLogging)
            Log.d(TAG, "onPause PlaceholderFragment");

        super.onPause();
    }

    @Override
    protected void onDestroy() {
        if(common.isLogging)
            Log.d(TAG, "onDestroy PlaceholderFragment");

        super.onDestroy();
    }
}