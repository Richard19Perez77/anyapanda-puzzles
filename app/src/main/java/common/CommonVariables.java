package common;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import audio.MySoundPool;
import logic.PuzzlePiece;
import logic.PuzzleSlot;

/**
 * A class to hold variables that will be used across classes and the more
 * commonly used Context object.
 * <p/>
 * Should be separated into common variables and common work.
 *
 * @author Rick
 */
public class CommonVariables {

    // /Setup up for commonly used variables that can be accessed when the class
    // is called
    private volatile static CommonVariables instance;

    public Context context;
    public int currentSoundPosition, tapSound, saveSound, numberOfPieces,
            currSlotOnTouchDown, currSlotOnTouchUp, inPlace, screenH, screenW,
            currentPuzzleImagePosition;
    public boolean chimeLoaded, evenlySplit, tapLoaded, movingPiece,
            isImageError, isPuzzleSolved, resumePreviousPuzzle, isImageLoaded,
            playChimeSound = true, drawBorders = true, playTapSound = true,
            playMusic = true;

    // the value is the piece to go into it
    public int[] slotOrder;
    public ArrayList<Integer> imagesShown = new ArrayList<>();
    public Random rand = new Random();
    public Resources res;
    public Bitmap image;
    public PuzzlePiece[] puzzlePieces;
    public PuzzleSlot[] puzzleSlots;
    public Button mNextButton;
    public ImageButton blogButton, wordpressLinkButton;
    public float volume;
    public MySoundPool mySoundPool;
    Toast toast = null;
    public double dimensions;
    public Point[] points;
    public int[] ys, xs;
    public boolean isWindowInFocus = false;
    // public boolean isScreenAnimating = false;

    public int piecesComplete;
    public int index;
    public boolean isPuzzleSplitCorrectly;
    public Date startPuzzle = new Date();
    public Date stopPuzzle = new Date();
    public long currPuzzleTime = 0;
    public boolean createNewPuzzle;

    public boolean isLogging = false;
    private static final String TAG = "puzzleLog";

    public static CommonVariables getInstance() {
        if (instance == null)
            synchronized (CommonVariables.class) {
                if (instance == null)
                    instance = new CommonVariables();
            }
        return instance;
    }

    public boolean setSlots(String string) {
        String[] stringSlots;
        stringSlots = string.split(",");
        slotOrder = new int[stringSlots.length];

        for (int i = 0; i < stringSlots.length; i++) {
            try {
                slotOrder[i] = Integer.parseInt(stringSlots[i]);
            } catch (NumberFormatException nfe) {
                return false;
            }
        }
        return true;
    }

    public boolean assignSlotOrder() {
        PuzzleSlot[] newSlots = new PuzzleSlot[numberOfPieces];
        for (int i = 0; i < numberOfPieces; i++) {
            newSlots[i] = new PuzzleSlot();
            newSlots[i].sx = puzzleSlots[i].sx;
            newSlots[i].sy = puzzleSlots[i].sy;
            newSlots[i].sx2 = puzzleSlots[i].sx2;
            newSlots[i].sy2 = puzzleSlots[i].sy2;
            newSlots[i].slotNum = puzzleSlots[i].slotNum;
            newSlots[i].puzzlePiece = puzzleSlots[slotOrder[i]].puzzlePiece;
            newSlots[i].puzzlePiece.px = newSlots[i].sx;
            newSlots[i].puzzlePiece.py = newSlots[i].sy;
        }

        for (PuzzleSlot newSlot : newSlots) {
            if (newSlot.slotNum != newSlot.puzzlePiece.pieceNum) {
                puzzleSlots = newSlots;
                return true;
            }
        }

        return false;
    }

    public int calculateInSampleSize(BitmapFactory.Options options,
                                     int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee a final image with both dimensions larger than or equal
            // to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                  int reqWidth, int reqHeight) {
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    // switch a piece in the puzzle
    public void sendPieceToNewSlot(int a, int z) {
        PuzzlePiece temp;
        new PuzzlePiece();
        temp = puzzleSlots[currSlotOnTouchDown].puzzlePiece;
        puzzleSlots[a].puzzlePiece = puzzleSlots[z].puzzlePiece;
        puzzleSlots[a].puzzlePiece.px = puzzleSlots[a].sx;
        puzzleSlots[a].puzzlePiece.py = puzzleSlots[a].sy;
        puzzleSlots[z].puzzlePiece = temp;
        puzzleSlots[z].puzzlePiece.px = puzzleSlots[z].sx;
        puzzleSlots[z].puzzlePiece.py = puzzleSlots[z].sy;
    }

    public void showToast(String message) {
        // Create and show toast for save photo
        Activity act = (Activity) context;
        if (toast == null) {
            toast = Toast.makeText(act, message, Toast.LENGTH_SHORT);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER, 0, 0);
        }
        if (!toast.getView().isShown()) {
            toast.setText(message);
            toast.show();
        } else {
            toast.cancel();
            toast.setText(message);
            toast.show();
        }
    }

    public void playSetSound() {
        // play sound for setting puzzle pieces
        mySoundPool.playSetSound();
    }

    public void initPrevDivideBitmap(int pieces) {
        numberOfPieces = pieces;

        points = new Point[pieces];

        puzzlePieces = new PuzzlePiece[pieces];
        for (int i = 0; i < numberOfPieces; i++) {
            puzzlePieces[i] = new PuzzlePiece();
        }

        puzzleSlots = new PuzzleSlot[pieces];
        for (int i = 0; i < numberOfPieces; i++) {
            puzzleSlots[i] = new PuzzleSlot();
        }
    }

    public void initDivideBitmap(int pieces) {
        numberOfPieces = pieces;

        points = new Point[pieces];

        // setup puzzle pieces with new pieces
        puzzlePieces = new PuzzlePiece[pieces];
        for (int i = 0; i < numberOfPieces; i++) {
            puzzlePieces[i] = new PuzzlePiece();
        }

        // setup for new slots for the pieces
        puzzleSlots = new PuzzleSlot[pieces];
        for (int i = 0; i < numberOfPieces; i++) {
            puzzleSlots[i] = new PuzzleSlot();
        }

        // default order for slots with perfect order
        slotOrder = new int[pieces];
        for (int i = 0; i < pieces; i++) {
            slotOrder[i] = i;
        }
    }

    public void showButtons() {
        if (isLogging)
            Log.d(TAG, "showButtons CommonVariables");

        Activity act = (Activity) context;
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mNextButton != null
                        && mNextButton.getVisibility() == View.INVISIBLE) {
                    mNextButton.setVisibility(View.VISIBLE);
                    mNextButton.bringToFront();
                }
                if (blogButton != null
                        && blogButton.getVisibility() == View.INVISIBLE) {
                    blogButton.setVisibility(View.VISIBLE);
                    blogButton.bringToFront();
                }
                if (wordpressLinkButton != null
                        && wordpressLinkButton.getVisibility() == View.INVISIBLE) {
                    wordpressLinkButton.setVisibility(View.VISIBLE);
                    wordpressLinkButton.bringToFront();
                }
            }
        });
    }

    public void hideButtons() {
        if (isLogging)
            Log.d(TAG, "hideButtons CommonVariables");

        Activity act = (Activity) context;
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mNextButton != null
                        && mNextButton.getVisibility() == View.VISIBLE) {
                    mNextButton.setVisibility(View.INVISIBLE);
                }
                if (blogButton != null
                        && blogButton.getVisibility() == View.VISIBLE) {
                    blogButton.setVisibility(View.INVISIBLE);
                }
                if (wordpressLinkButton != null
                        && wordpressLinkButton.getVisibility() == View.VISIBLE) {
                    wordpressLinkButton.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    public void toggleUIOverlay() {
        if (isLogging)
            Log.d(TAG, "toggleUIOverlay CommonVariables");

        Activity act = (Activity) context;
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mNextButton != null)
                    if (mNextButton.getVisibility() == View.VISIBLE)
                        mNextButton.setVisibility(View.INVISIBLE);
                    else {
                        mNextButton.setVisibility(View.VISIBLE);
                        mNextButton.bringToFront();
                    }

                if (blogButton != null)
                    if (blogButton.getVisibility() == View.VISIBLE)
                        blogButton
                                .setVisibility(View.INVISIBLE);
                    else {
                        blogButton
                                .setVisibility(View.VISIBLE);
                        blogButton.bringToFront();
                    }

                if (wordpressLinkButton != null)
                    if (wordpressLinkButton
                            .getVisibility() == View.VISIBLE)
                        wordpressLinkButton
                                .setVisibility(View.INVISIBLE);
                    else {
                        wordpressLinkButton
                                .setVisibility(View.VISIBLE);
                        wordpressLinkButton.bringToFront();
                    }
            }
        });
    }
}